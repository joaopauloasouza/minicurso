
-- Table 'usuario'
-- ------------------------------------------------------------------
create table `usuario` (
    `id` int(11) unsigned not null auto_increment,
    `nome` varchar(50) not null,
    `email` varchar(100) not null,
    `senha` char(40) not null,
    constraint `pk_usuario` primary key (`id`),
    constraint `uk_usuario_email` unique (`email`)
) engine=InnoDB default charset=utf8 collate=utf8_general_ci;
-- ------------------------------------------------------------------


-- Table 'noticia'
-- ------------------------------------------------------------------
create table `noticia` (
    `id` int(11) unsigned not null auto_increment,
    `titulo` varchar(128) not null,
    `slug` varchar(128) not null,
    `conteudo` text not null,
    `data` timestamp not null default current_timestamp,
    `usuario` int unsigned not null,
    constraint `pk_noticia` primary key (`id`),
    constraint `uk_noticia_titulo` unique (`titulo`),
    constraint `pk_noticia_usuario` foreign key (`usuario`) references `usuario` (`id`),
    KEY `slug_idx` (`slug`),
    KEY `usuario_idx` (`usuario`)
) engine=InnoDB default charset=utf8 collate=utf8_general_ci;
-- ------------------------------------------------------------------



INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`) VALUES (1, 'João Paulo Angeleti de Souza', 'joaopauloasouza@hotmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b');