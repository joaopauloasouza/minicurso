<div class="list-group">
    <a href="<?php echo base_url('admin'); ?>" class="list-group-item <?php echo ($this->uri->segment(2) == '') ? 'active' : ''; ?>">
        <i class="fa fa-fw fa-home"></i>&nbsp;Home
    </a>

    <a href="<?php echo base_url('admin/noticias'); ?>" class="list-group-item <?php echo ($this->uri->segment(2) === 'noticias') ? 'active' : ''; ?>">
        <i class="fa fa-fw fa-newspaper-o"></i>&nbsp;Notícias
    </a>

    <a href="<?php echo base_url('admin/perfil'); ?>" class="list-group-item <?php echo ($this->uri->segment(2) === 'perfil') ? 'active' : ''; ?>">
        <i class="fa fa-fw fa-user"></i>&nbsp;Perfil
    </a>
</div>