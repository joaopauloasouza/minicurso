<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Alternar navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="<?php echo base_url('admin'); ?>">Blog Admin</a>
        </div><!-- /.navbar-header -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="navbar" class="collapse navbar-collapse">
            <a href="<?php echo base_url('contas/deslogar'); ?>" class="btn btn-default btn-custom navbar-btn navbar-right">
                <i class="fa fa-sign-out"></i>&nbsp;Sair
            </a>

            <p class="navbar-text navbar-right"><?php echo $this->session->userdata('username'); ?></a></p>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>