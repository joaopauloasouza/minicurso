<ol class="breadcrumb">
    <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-home"></i>&nbsp;Home</a></li>
    <li><a href="<?php echo base_url('admin/noticias'); ?>">Notícias</a></li>
    <li class="active">Cadastrar Notícia</li>
</ol>

<?php echo form_open('admin/noticias/salvar', 'role="form"'); ?>
<div class="form-group">
    <label for="titulo">Título:</label>
    <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo set_value('titulo'); ?>" maxlength="128" autofocus>
    <?php echo form_error('titulo'); ?>
</div>

<div class="form-group">
    <label for="conteudo">Conteúdo:</label>
    <textarea class="form-control" name="conteudo" id="conteudo" rows="10"><?php echo set_value('conteudo'); ?></textarea>
    <?php echo form_error('conteudo'); ?>
</div>

<p><button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Salvar Notícia</button></p>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(function() {
        $('#conteudo').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol']],
                ['insert', ['link', 'hr']],
                ['misc', ['undo', 'redo', 'fullscreen', 'help']]
            ],
            lang: "pt-BR"
        });
    });
</script>