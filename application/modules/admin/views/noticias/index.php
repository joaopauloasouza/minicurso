<ol class="breadcrumb">
    <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-home"></i>&nbsp;Home</a></li>
    <li class="active">Notícias</li>
</ol>

<div class="message"><?php echo $this->session->flashdata('alert'); ?></div>


<div class="panel panel-default">
    <div class="panel-heading">
        Lista de Notícias
        <a href="<?php echo base_url('admin/noticias/cadastrar'); ?>" class="btn btn-success pull-right">Nova Notícia</a>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-right">#</th>
                    <th>Título</th>
                    <th class="text-center">Data</th>
                    <th class="text-center"><i class="fa fa-gears"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($noticias as $n): ?>
                    <tr>
                        <td class="text-right col-sm-1"><?php echo $n['id']; ?></td>
                        <td class="text-left col-sm-8"><a href="#"><?php echo word_limiter($n['titulo'], 10); ?></a></td>
                        <td class="text-center col-sm-2"><?php echo date_to_br($n['data']); ?></td>
                        <td class="text-center col-sm-1"><a href="#"><i class="fa fa-edit"></i></a></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>