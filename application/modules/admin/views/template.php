<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="#">
    <meta name="author" content="#">
    <link rel="icon" href="#">

    <title><?php echo $title; ?></title>

    <!-- Font Awesome core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/font-awesome/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <!-- Summernote core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/summernote/summernote.css">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/admin.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>public/js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <!-- Summernote core JavaScript -->
    <script src="<?php echo base_url(); ?>public/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url(); ?>public/summernote/lang/summernote-pt-BR.js"></script>
</head>
<body>
    <!-- Navigation -->
    <?php $this->load->view('navbar'); ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-3"><?php $this->load->view('sidebar'); ?></div>

            <div class="col-lg-9"><?php echo $contents; ?></div>
        </div>
    </div>
</body>
</html>