<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia_model extends MY_Model {

    protected $table_name = 'noticia';
    protected $primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $order_by = 'titulo';

}

/* End of file Noticia_model.php */
/* Location: ./application/modules/admin/models/Noticia_model.php */
