<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('noticia_model');
    }


    public function index()
    {
        $data = array(
            'title'    => "Notícias",
            'noticias' => $this->noticia_model->find(array('usuario' => $this->user))
        );

        $this->template->load('template', './noticias/index', $data);
    }

    public function cadastrar()
    {
        $data = array(
            'title' => "Cadastrar Notícia"
        );

        $this->template->load('template', './noticias/cadastrar', $data);
    }

    public function salvar()
    {
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        $this->form_validation->set_rules('titulo', 'Título', 'trim|required|is_unique[noticia.titulo]|max_length[128]');
        $this->form_validation->set_rules('conteudo', 'Conteúdo', 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->cadastrar();
        } else {
            $slug = convert_accented_characters($this->input->post('titulo'));

            $data = array(
                'titulo'   => $this->input->post('titulo'),
                'slug'     => url_title($slug, 'dash', TRUE),
                'conteudo' => $this->input->post('conteudo'),
                'usuario'  => $this->user
            );

            $this->noticia_model->insert($data);
            $this->session->set_flashdata('alert', alert_success("Notícia cadastrada com sucesso!"));
            redirect('admin/noticias');
        }
    }

}

/* End of file Noticias.php */
/* Location: ./application/modules/admin/controllers/Noticias.php */
