<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }


    public function index()
    {
        $data = array(
            'title' => "Administração do Blog"
        );

        $this->template->load('template', './admin/index', $data);
    }

}

/* End of file Admin.php */
/* Location: ./application/modules/admin/controllers/Admin.php */
