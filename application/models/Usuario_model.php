<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends MY_Model {

    protected $table_name = 'usuario';
    protected $primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $order_by = 'nome';

}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */
