<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->library('form_validation');
        $this->load->helper('alert');
    }


    public function index()
    {
        $data = array(
            'title' => "Entrar"
        );

        $this->load->view('contas/index', $data);
    }


    public function logar()
    {
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->index();
        } else {
            $email = strtolower($this->input->post('email'));
            $senha = sha1($this->input->post('senha'));

            $where = array('email' => $email, 'senha' => $senha);
            $userdata = $this->usuario_model->find($where, TRUE);

            if (! empty($userdata)) {
                $data = array(
                    'user'     => $userdata['id'],
                    'username' => $userdata['nome'],
                    'logged'   => TRUE
                );

                $this->session->set_userdata($data);
                redirect('admin', 'refresh');
            } else {
                $this->session->set_flashdata('alert', alert_danger("Usuário não encontrado!"));
                redirect('contas');
            }
        }
    }

    public function deslogar()
    {
        $this->session->unset_userdata('logged');
        $this->session->sess_destroy();
        redirect('contas', 'refresh');
    }

}

/* End of file Contas.php */
/* Location: ./application/controllers/Contas.php */
