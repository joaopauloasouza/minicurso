<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends CI_Controller {

    protected $user;

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');

        $this->load->library('form_validation');

        $this->load->helper('alert');
        $this->load->helper('converter');
        $this->load->helper('text');

        if ($this->session->userdata('logged') == FALSE) {
            $this->session->set_flashdata('alert', alert_danger("Efetue o seu Login!"));
            redirect('contas');
        }
    }

}

/* End of file Admin_Controller.php */
/* Location: ./application/core/Admin_Controller.php */
