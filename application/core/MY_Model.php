<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    protected $table_name = '';
    protected $primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $order_by = '';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [get description]
     * @param  boolean       $single [TRUE if you want to get a single row]
     * @return array                 [single or multiple rows]
     */
    public function get($single = FALSE)
    {
        $method = ($single == TRUE) ? 'row_array' : 'result_array';

        $this->db->order_by($this->order_by);
        return $this->db->get($this->table_name)->$method();
    }


    /**
     * [find description]
     * @param  array         $where  [vector fields to be compared]
     * @param  boolean       $single [TRUE if you want to get a single row]
     * @return array                 [single or multiple rows]
     */
    public function find($where, $single = FALSE)
    {
        $this->db->where($where);
        return $this->get($single);
    }


    /**
     * [findById description]
     * @param  integer $id [receives the primary key of the object that you want to search]
     * @return array       [returns the object related to the primary key specified as a parameter]
     */
    public function findById($id)
    {
        if (!isset($id)) return false;

        $filter = $this->primary_filter;
        $id = $filter($id);

        $this->db->where($this->primary_key, $id);
        $this->db->limit(1);
        return $this->db->get($this->table_name)->row_array();
    }


    /**
     * [insert description]
     * @param  object  $data [receives the object you want to insert]
     * @return integer       [returns the primary key of the inserted object]
     */
    public function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }


    /**
     * [update description]
     * @param  object  $data [receives the object to be updated along with the primary key]
     * @return integer       [returns the primary key of the current object]
     */
    public function update($data)
    {
        if (!isset($data[$this->primary_key])) return false;

        $filter = $this->primary_filter;
        $id = $filter($data[$this->primary_key]);

        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data);
        return $id;
    }


    /**
     * [delete description]
     * @param  integer $id [description]
     * @return boolean     [description]
     */
    public function delete($id)
    {
        if (!isset($id)) return false;

        $filter = $this->primary_filter;
        $id = $filter($id);

        $this->db->where($this->primary_key, $id);
        $this->db->limit(1);
        return $this->db->delete($this->table_name);
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
