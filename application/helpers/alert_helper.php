<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Alert Success
 *
 * @param  string $message [message]
 * @return string          [return success message]
 */
if (!function_exists('alert_success'))
{
    function alert_success($message)
    {
        $result =   '<div class="alert alert-success alert-dismissible" role="alert">' .
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>' .
                        '<strong><i class="fa fa-check-circle"></i>&nbsp;Sucesso!</strong> ' . $message .
                    '</div>';

        return $result;
    }
}


/**
 * Alert Danger
 *
 * @param  string $message [message]
 * @return string          [return danger message]
 */
if (!function_exists('alert_danger'))
{
    function alert_danger($message)
    {
        $result =   '<div class="alert alert-danger alert-dismissible" role="alert">' .
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>' .
                        '<strong><i class="fa fa-minus-circle"></i>&nbsp;Erro!</strong> ' . $message .
                    '</div>';

        return $result;
    }
}


/**
 * Alert Info
 *
 * @param  string $message [message]
 * @return string          [return information message]
 */
if (!function_exists('alert_info'))
{
    function alert_info($message)
    {
        $result =   '<div class="alert alert-info alert-dismissible" role="alert">' .
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>' .
                        '<strong><i class="fa fa-info-circle"></i>&nbsp;Informativo!</strong> ' . $message .
                    '</div>';

        return $result;
    }
}


/**
 * Alert Warning
 *
 * @param  string $message [message]
 * @return string          [return warning message]
 */
if (!function_exists('alert_warning'))
{
    function alert_warning($message)
    {
        $result =   '<div class="alert alert-warning alert-dismissible" role="alert">' .
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>' .
                        '<strong><i class="fa fa-exclamation-circle"></i>&nbsp;Atenção!</strong> ' . $message .
                    '</div>';

        return $result;
    }
}


/* End of file alert_helper.php */
/* Location: ./application/helpers/alert_helper.php */
