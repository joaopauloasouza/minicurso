<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Remove Phone Mask
 * @param  string $str [Phone number with mask]
 * @return string      [only numbers]
 */
if (!function_exists('remove_phone_mask'))
{
    function remove_phone_mask($str)
    {
        $str = preg_replace('[\D]', '', $str);
        return $str;
    }
}



/**
 * Date to US
 * @param  string $str [BR date format]
 * @return string      [US date format]
 */
if (!function_exists('date_to_us'))
{
    function date_to_us($str)
    {
        $year = substr($str, 6, 4);
        $month = substr($str, 3, 2);
        $day = substr($str, 0, 2);
        return $year . '-' . $month . '-' . $day;
    }
}

/**
 * Datetime to US
 * @param  string $str [BR datetime format]
 * @return string      [US datetime format]
 */
if (!function_exists('datetime_to_us'))
{
    function datetime_to_us($str)
    {
        $year = substr($str, 6, 4);
        $month = substr($str, 3, 2);
        $day = substr($str, 0, 2);
        $hour = substr($str, 11, 8);
        return $year . '-' . $month . '-' . $day . ' ' . $hour;
    }
}



/**
 * Date to BR
 * @param  string $str [US date format]
 * @return string      [BR date format]
 */
if (!function_exists('date_to_br'))
{
    function date_to_br($str)
    {
        $year = substr($str, 0, 4);
        $month = substr($str, 5, 2);
        $day = substr($str, 8, 2);
        return $day . '/' . $month . '/' . $year;
    }
}

/**
 * Datetime to BR
 * @param  string $str [US datetime format]
 * @return string      [BR datetime format]
 */
if (!function_exists('datetime_to_br'))
{
    function datetime_to_br($str)
    {
        $year = substr($str, 0, 4);
        $month = substr($str, 5, 2);
        $day = substr($str, 8, 2);
        $hour = substr($str, 11, 8);
        return $day . '/' . $month . '/' . $year . ' ' . $hour;
    }
}



/**
 * BRL to Decimal
 * @param  string $str [BRL currency format]
 * @return string      [Decimal format]
 */
if (!function_exists('brl_to_decimal'))
{
    function brl_to_decimal($str)
    {
        $str = str_replace('.', '', $str);
        $str = str_replace(',', '.', $str);
        return $str;
    }
}

/**
 * Decimal to BRL
 * @param  string $str [Decimal format]
 * @return string      [BRL currency format]
 */
if (!function_exists('decimal_to_brl'))
{
    function decimal_to_brl($str)
    {
        $str = str_replace(',', '', $str);
        $str = str_replace('.', ',', $str);
        return $str;
    }
}


/* End of file converter_helper.php */
/* Location: ./application/helpers/converter_helper.php */
