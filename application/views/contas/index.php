<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="#">
    <meta name="author" content="#">
    <link rel="icon" href="#">

    <title><?php echo $title; ?></title>

    <!-- Font Awesome core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/font-awesome/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/contas.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container form-body">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><strong>Entrar</strong></h3>
                    <div class="form-forgot"><a href="#">Esqueceu sua senha?</a></div>
                </div>

                <div class="panel-body">
                    <?php echo form_open('contas/logar', 'role="form"'); ?>
                        <div class="form-alert"><?php echo $this->session->flashdata('alert'); ?></div>

                        <div class="form-group">
                            <label class="sr-only" for="email">Email:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-user" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                            <?php echo form_error('email'); ?>
                        </div>

                        <div class="form-group">
                            <label class="sr-only" for="senha">Senha:</label>
                            <div class="input-group input-control">
                                <span class="input-group-addon"><i class="fa fa-fw fa-lock" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha">
                            </div>
                            <?php echo form_error('senha'); ?>
                        </div>

                        <button type="submit" class="btn btn-success btn-block"><i class="fa fa-sign-in"></i>&nbsp;Entrar</button>

                        <hr class="form-hr">

                        <div class="form-group">
                            <div class="form-bottom">
                                <span>Não tem uma conta!</span>
                                <a href="#">Registre-se aqui</a>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div><!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>public/js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
</body>
</html>