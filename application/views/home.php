<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="#">
    <meta name="author" content="#">
    <link rel="icon" href="#">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/blog.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                    <span class="sr-only">Alternar navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.navbar-header -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($this->uri->segment(1) === 'home') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('home'); ?>">Home</a>
                    </li>

                    <li class="<?php echo ($this->uri->segment(1) === 'sobre') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('sobre'); ?>">Sobre</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

    <div class="container">
        <div class="blog-header">
            <h1 class="blog-title">The Bootstrap Blog</h1>
            <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
        </div>

        <div class="row">
            <div class="col-sm-8 blog-main">
                <div class="blog-post">
                    <h2 class="blog-post-title">Sample Blog Post</h2>
                    <p class="blog-post-meta">Januray 1, 2014 by <a href="#">John Doe</a></p>
                    <div class="blog-post-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore aliquam adipisci eaque libero nihil id aliquid, incidunt aspernatur accusamus, obcaecati, magni explicabo aperiam repellendus. Et quia blanditiis iste facilis consequatur.</p>
                    </div>
                    <p><a href="#" class="btn btn-link">Leia mais</a></p>
                </div><!-- /.blog-post -->

                <div class="blog-post">
                    <h2 class="blog-post-title">Another Sample Blog Post</h2>
                    <p class="blog-post-meta">Januray 1, 2014 by <a href="#">John Doe</a></p>
                    <div class="blog-post-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore aliquam adipisci eaque libero nihil id aliquid, incidunt aspernatur accusamus, obcaecati, magni explicabo aperiam repellendus. Et quia blanditiis iste facilis consequatur.</p>
                    </div>
                    <p><a href="#" class="btn btn-link">Leia mais</a></p>
                </div><!-- /.blog-post -->

                <nav>
                    <ul class="pager">
                        <li><a href="#">Anterior</a></li>
                        <li><a href="#">Próximo</a></li>
                    </ul>
                </nav>
            </div><!-- /.blog-main -->

            <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
                <div class="sidebar-module sidebar-module-inset">
                    <h4>Sobre</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, eveniet dolore est. Libero, neque, doloribus!</p>
                </div>

                <div class="sidebar-module">
                    <h4>Arquivos</h4>

                    <ol class="list-unstyled">
                        <li><a href="#">March 2014</a></li>
                        <li><a href="#">February 2014</a></li>
                        <li><a href="#">January 2014</a></li>
                        <li><a href="#">December 2013</a></li>
                    </ol>
                </div>

                <div class="sidebar-module">
                    <h4>Outros Contatos</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">GitHub</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ol>
                </div>
            </div><!-- /.blog-sidebar -->
        </div>
    </div>

    <footer class="blog-footer">
        <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
        <p><a href="#">Back to top</a></p>
    </footer>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>public/js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
</body>
</html>